<?PHP include '../partials/common_header.php' ?>

<?php

$results = null;

if ($conn) {
    $sql = "select * from Item";

    $results = $conn->query($sql);
}
?>



        <!-- Page Content -->
        <div class="container">

            <div class="row">

                <div class="col-md-3">
                    <p class="lead">Shop Name</p>
                    <div class="list-group">
                        <a href="#" class="list-group-item">Category 1</a>
                        <a href="#" class="list-group-item">Category 2</a>
                        <a href="#" class="list-group-item">Category 3</a>
                    </div>
                </div>

                <div class="col-md-9">

                    <div class="row carousel-holder">

                        <div class="col-md-12">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                    </div>
                                    <div class="item">
                                        <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                    </div>
                                    <div class="item">
                                        <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                    </div>
                                </div>
                                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                    <?PHP
                    while ($item = $results->fetch_assoc()) {
                        $dbId = $item["ItemID"];
                        $dbTitle = $item["Title"];
                        $dbPrice = $item["Price"];
                        $dbCategory = $item["CategoryID"];
                        $dbShortDescription = $item["ShortDescription"];
                        $dbImageURL = $item["ImageURL"];
                        ?>
                            <div class="col-sm-4 col-lg-4 col-md-4">
                                <div class="thumbnail">
                                    <img src="<?PHP echo $dbImageURL; ?>" alt="">
                                    <div class="caption">
                                        <h4 class="pull-right">VND <?PHP echo $dbPrice; ?></h4>
                                        <h4><a href="<?PHP echo "/FoodSpot/php/fooddetails.php?id=$dbId" ?>"><?PHP echo $dbTitle; ?></a>
                                        </h4>
                                        <p><?php echo $dbShortDescription; ?></p>
                                    </div>
<!--                                    <div class="ratings">
                                        <p class="pull-right">15 reviews</p>
                                        <p>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                        </p>
                                    </div>-->
                                </div>
                            </div>

                        <?PHP } ?>





                    </div>

                </div>

            </div>

        </div>
        <!-- /.container -->

<?PHP include '../partials/common_footer.php' ?>
