<?php

session_start();

function connectToDB() {
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "FoodSpot";

    // Create connection
    return $conn = mysqli_connect($servername, $username, $password, $dbname);
}

function isUserLogined() {
    $cookie_name = "loginEmail";

    if (isset($_COOKIE[$cookie_name]) || isset($_SESSION[$cookie_name]))
        return true;
    else
        return false;
}

function isUserAdmin() {
    $cookie_name = "loginEmail";

    if (isset($_COOKIE[$cookie_name]) || isset($_SESSION[$cookie_name])) {
        $userName = isset($_COOKIE[$cookie_name]) == true ? $_COOKIE[$cookie_name] : $_SESSION[$cookie_name];
        if($userName == "admin@foodspot.vn")
            return true;
        else
            return false;
    } else
        return false;
}

function signout() {
    $cookie_name = "loginEmail";

    setcookie($cookie_name, "", time() - 3600, "/");

    $_SESSION[$cookie_name] = null;
}

function parse($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function containOnlyAlphabet($data) {
    return preg_match("/^[a-zA-Z ]*$/", $data);
}

function isEmailFormat($data) {
    return filter_var($data, FILTER_VALIDATE_EMAIL);
}

function isURLFormat($data) {
    return filter_var($data, FILTER_VALIDATE_URL);
}

function uploadImageFile( $target_dir, $target_file) {
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
    $uploadFolder = $target_dir . "/" . $target_file;
    // Check if image file is a actual image or fake image
    if (isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileupload"]["tmp_name"]);
        if ($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["fileupload"]["size"] > 500000) { // 500kb
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileupload"]["tmp_name"], $uploadFolder)) {
            echo "The file " . basename($_FILES["fileupload"]["name"]) . " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
}

?>
