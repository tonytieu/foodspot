<?php include '../partials/login_signup_header.php'; ?>
<?php include './utility.php' ?>

<?php
    // Create connection
    $conn = connectToDB();
    $sql = "select * from User;";

    // Check connection
    if ($conn->connect_error) {

        echo "Connection failed: " . $conn->connect_error;
    } else {
        //echo "Connected successfully";
    }
?>

<?php

    echo '<br />';

    $results = $conn->query($sql);

    $isFound = false;

    $username = "";
    $pass = "";
    $rememberme = null;


    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $username = $_POST["email"];
        $pass = $_POST["password"];
//        echo implode(" ", $_POST["step"]);
        
        if (isset($_POST["rememberme"])) {
            $rememberme = $_POST["rememberme"];
        }

        if ($results->num_rows) {

            while ($user = $results->fetch_assoc()) {
                $dbemail = $user["Email"];
                $dbpassword = $user["Password"];

                if ($username == $dbemail && $pass == $dbpassword) {
                    $isFound = true;
                    break;
                } else {
                    $isFound = false;
                };
            }
        }


        if ($isFound) {
            $cookie_name = "loginEmail";
            $cookie_value = $username;

            if (isset($rememberme)) {
                setcookie($cookie_name, $cookie_value, time() + (86400 * 1), "/"); // 86400 = 1 day
            }

            $_SESSION[$cookie_name] = $cookie_value;
            echo 'Login successfully';
            
            if($username == "admin@foodspot.vn") {
                header('Location: ' . "/FoodSpot/admin");
            } else {
                header('Location: ' . "/FoodSpot/");
            }
        } else {
            echo '<p class="bg-danger">Either username or password is invalid </p>';
        }
    }
?>


<div class="login-panel panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Please Sign In</h3>
    </div>
    <div class="panel-body">
        <form role="form" method="POST" action="<?php echo $_SERVER["PHP_SELF"] ?>">
            <fieldset>
                <div class="form-group">
                    <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus 
                           value="<?php echo $username; ?>">
                </div>
                <div class="form-group">
                    <input class="form-control" placeholder="Password" name="password" type="password" 
                           value="<?php echo $pass; ?>">
                </div>
                <div class="checkbox">
                    <label>
                        <input name="rememberme" type="checkbox" value="Remember Me">Remember Me
                    </label>
                </div>
                
<!--                <input name="step[]" />
                <input name="step[]" />
                <input name="step[]" />-->
                <!-- Change this to a button or input when using this as a form -->
                <button class="btn btn-lg btn-success btn-block">Login</button>
            </fieldset>
        </form>
    </div>
</div>

