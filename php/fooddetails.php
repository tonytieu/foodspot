<?php
    include '../partials/common_header.php';
    
    $itemId = null;
    $results = null;
    
    if(isset($_GET["id"])) {
        $itemId = $_GET["id"];
        
        $sql = "select * from Item where ItemID = " . $itemId;
        
        if($conn) {
            $results = $conn->query($sql);
        }
    }
    
?>

<!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Shop Name</p>
                <div class="list-group">
                    <a href="#" class="list-group-item active">Category 1</a>
                    <a href="#" class="list-group-item">Category 2</a>
                    <a href="#" class="list-group-item">Category 3</a>
                </div>
            </div>

            <div class="col-md-9">
                <?PHP 
                    while($item = $results->fetch_assoc()) {    
                ?>
                        <div class="thumbnail">
                            <img class="img-responsive" src="<?PHP echo $item["ImageURL"]; ?>" alt="">
                            <div class="caption-full">
                                <h4 class="pull-right">VND<?PHP echo $item["Price"]; ?></h4>
                                <h4><a href="#"><?PHP echo $item["Title"]; ?></a>
                                </h4>
                                <p>
                                    <?PHP echo $item["LongDescription"]; ?>
                                </p>
                            </div>
<!--                            <div class="ratings">
                                <p class="pull-right">3 reviews</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    4.0 stars
                                </p>
                            </div>-->
                        </div>
                <?PHP } ?>

<!--                <div class="well">

                    <div class="text-right">
                        <a class="btn btn-success">Leave a Review</a>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star-empty"></span>
                            Anonymous
                            <span class="pull-right">10 days ago</span>
                            <p>This product was great in terms of quality. I would definitely buy another!</p>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star-empty"></span>
                            Anonymous
                            <span class="pull-right">12 days ago</span>
                            <p>I've alredy ordered another one!</p>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star-empty"></span>
                            Anonymous
                            <span class="pull-right">15 days ago</span>
                            <p>I've seen some better than this, but not at this price. I definitely recommend this item.</p>
                        </div>
                    </div>

                </div>-->

            </div>

        </div>

    </div>
    <!-- /.container -->

<?php
    include '../partials/common_footer.php'
?>

