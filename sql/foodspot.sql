-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2016 at 01:55 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `foodspot`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `CategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) NOT NULL,
  PRIMARY KEY (`CategoryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`CategoryID`, `Name`) VALUES
(1, 'category1'),
(2, 'category2');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `ItemID` int(11) NOT NULL AUTO_INCREMENT,
  `LongDescription` text,
  `Title` varchar(200) NOT NULL,
  `Price` float DEFAULT NULL,
  `CategoryID` int(200) DEFAULT NULL,
  `ShortDescription` text NOT NULL,
  `ImageURL` varchar(1000) DEFAULT NULL,
  `DateCreated` date DEFAULT NULL,
  `DateModified` date DEFAULT NULL,
  PRIMARY KEY (`ItemID`),
  KEY `CategoryID` (`CategoryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`ItemID`, `LongDescription`, `Title`, `Price`, `CategoryID`, `ShortDescription`, `ImageURL`, `DateCreated`, `DateModified`) VALUES
(6, NULL, 'food1', NULL, 1, 'desasdfa1', '/FoodSpot/admin/upload/food/foody-mobile-pho-thin-lo-duc.jpg', NULL, NULL),
(7, NULL, 'fooid2', NULL, 2, 'descrip3', '/FoodSpot/admin/upload/food/foody-mobile-pho-thin-lo-duc.jpg', NULL, NULL),
(8, NULL, 'food43', NULL, 2, 'asdfasdf', '/FoodSpot/admin/upload/food/foody-mobile-pho-thin-lo-duc.jpg', NULL, NULL),
(9, NULL, 'food2134', NULL, 2, 'asdfasdf', 'C:/Xampp/htdocs/FoodSpot/admin/upload/food/foody-mobile-pho-thin-lo-duc.jpg', NULL, NULL),
(10, NULL, 'food4', NULL, 2, 'sadfasdfg', '/FoodSpot/admin/upload/food/foody-mobile-pho-thin-lo-duc.jpg', NULL, NULL),
(11, NULL, 'sdfg', NULL, 1, 'sdfg', '/FoodSpot/admin/upload/food/foody-mobile-pho-thin-lo-duc.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_image`
--

CREATE TABLE IF NOT EXISTS `item_image` (
  `ItemImageID` int(11) NOT NULL AUTO_INCREMENT,
  `URL` varchar(2000) NOT NULL,
  `ItemID` int(11) NOT NULL,
  PRIMARY KEY (`ItemImageID`),
  KEY `ItemID` (`ItemID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(200) NOT NULL,
  `Password` varchar(500) NOT NULL,
  `FirstName` varchar(200) DEFAULT NULL,
  `LastName` varchar(200) DEFAULT NULL,
  `Address` varchar(200) DEFAULT NULL,
  `IsUserBlocked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`UserID`, `Email`, `Password`, `FirstName`, `LastName`, `Address`, `IsUserBlocked`) VALUES
(1, 'admin@foodspot.vn', '123456789', '', '', NULL, 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `FK_Item_Category_CategoryID` FOREIGN KEY (`CategoryID`) REFERENCES `category` (`CategoryID`);

--
-- Constraints for table `item_image`
--
ALTER TABLE `item_image`
  ADD CONSTRAINT `FK_Item_Image_Item_ItemID` FOREIGN KEY (`ItemID`) REFERENCES `item` (`ItemID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
