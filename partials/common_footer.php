        <div class="container">

            <hr>

            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; Your Website 2014</p>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->

        <!-- jQuery -->
        <script src="../plugins/jquery/jquery-2.2.0.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../plugins/bootstrap/js/bootstrap.min.js"></script>

        <script>
<?PHP if ($conn) { ?>
                console.log("database connected");
<?PHP } else { ?>
                console.log("database not connected");
<?PHP } ?>
        </script>
    </body>

</html>