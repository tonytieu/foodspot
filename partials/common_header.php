<?PHP include './utility.php'; ?>

<?PHP 
    $conn = connectToDB();
?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>FoodSpot Homepage</title>

        <!-- Bootstrap Core CSS -->
        <link href="../plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/shop-homepage.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">FoodSpot</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">Page 1</a></li>
                    <li><a href="#">Page 2</a></li> 
                    <li><a href="#">Page 3</a></li> 
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php if (isUserLogined()) { ?>
                        <li><a href="/FoodSpot/php/signout.php"><span class="glyphicon glyphicon-user"></span> Sign Out</a></li>
                        <?PHP
                            if(isUserAdmin()) {
                                echo '<li><a href="/FoodSpot/admin/"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>';
                            }
                        ?>
                    <?php } else { ?>
                        <li><a href="/FoodSpot/php/signup.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li><a href="/FoodSpot/php/login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    <?php } ?>
                </ul>
            </div>
        </nav>