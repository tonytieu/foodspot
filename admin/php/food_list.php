

<?php include '../partials/header.php'; ?>

<?PHP 
    $conn = connectToDB();
?>


<?php

$results = null;

if ($conn) {
    $sql = "select * from Item";

    $results = $conn->query($sql);
}

if(isset($_GET["DeletedItemID"])) {
    if($conn) {
         $sql = "delete from ITEM where ItemID = " . $_GET["DeletedItemID"];
         if ($conn->query($sql) === TRUE) {
            echo "Record deleted successfully";
            
        } else {
            echo "Error deleting record: " . $conn->error;
        }
    }
}

?>


<h2>Food List</h2>

<table class="table table-condensed table-hover table-bordered table-striped">
    <tr>
        <td>ItemID</td>
        <td>Title</td>
        <td>Price</td>
        <td>Category</td>
        <td>ShortDescription</td>
        <td>Image</td>
    </tr>
<?PHP
    while ($item = $results->fetch_assoc()) {
        $dbId = $item["ItemID"];
        $dbTitle = $item["Title"];
        $dbPrice = $item["Price"];
        $dbCategory = $item["CategoryID"];
        $dbShortDescription = $item["ShortDescription"];
        $dbImageUrl = $item["ImageURL"];
        ?>
        
    <tr>
        <td><?PHP echo $dbId; ?></td>
        <td><?PHP echo $dbTitle; ?></td>
        <td><?PHP echo $dbPrice; ?></td>
        <td><?PHP echo $dbCategory; ?></td>
        <td><?PHP echo $dbShortDescription; ?></td>
        <td><img src="<?php echo $dbImageUrl; ?>" alt="<?php echo $dbTitle; ?>" style="width: 200px; height: 200px;" /></td>
        <td>
            <a href="food_edit.php?ItemID=<?PHP echo $dbId;?>">Edit</a>
            <a href="<?PHP echo $_SERVER['PHP_SELF']; ?>?DeletedItemID=<?PHP echo $dbId;?>" class="btnDelete">Delete</a>
        </td>
    </tr>

        <?PHP } ?>
</table>


<?php include '../partials/footer.php'; ?>

<script>
    $(document).ready(function(){
        $(".btnDelete").on("click", function(event){
            event.stopPropagation();
            
            var r = confirm("Are you sure?");
            if (!r) {
                event.preventDefault();
                return false;
            }
        });
    });
</script>
