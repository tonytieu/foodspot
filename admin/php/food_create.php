<?php include '../partials/header.php'; ?>

<?PHP 

    $conn = connectToDB();
    $result = null;
    
    if ($conn) {
        $sql = "select * from CATEGORY";

        $results = $conn->query($sql);
    }
    
?>

<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    //$dateCreated = date();
    $title = $_POST["title"];
    $description = $_POST["shortdescription"];
    $catetory = $_POST["category"];
    $image = $_FILES["fileupload"]["tmp_name"];
    $target_dir  = '/FoodSpot/admin/upload/food';
    $target_file = $_FILES["fileupload"]["name"];
    
    $imagePath = $target_dir . "/" . $target_file;
    
    uploadImageFile($_SERVER['DOCUMENT_ROOT'] . $target_dir, $target_file);

//    echo '$title: ' . $title;
//    echo '$description: ' . $description;
//    echo '$catetories' . $catetory;
//    echo 'image temp path: ' . $image;
    
    $sql = "insert into ITEM(Title, ShortDescription, CategoryID, ImageURL, DateCreated) "
            . "values('$title', '$description' , $catetory, '$imagePath', now())";
    
    if (mysqli_query($conn, $sql)) {
        $last_id = mysqli_insert_id($conn);
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

}
?>

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Create new food</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?PHP echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data" role="form">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" value="" class="form-control" />
                                <p class="help-block">Example block-level help text here.</p>
                            </div>

                            <div class="form-group">
                                <label>Short Description</label>
                                <textarea class="form-control" cols="5" rows="5" name="shortdescription"></textarea>
                            </div>

                            <div class="form-group">
                                <label>Categories</label>
                                
                                <?PHP
                                     while ($item = $results->fetch_assoc()) {
                                        $dbId = $item["CategoryID"];
                                        $dbName = $item["Name"];                                  
                                ?>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="category" value="<?php echo $dbId; ?>"> 
                                                    <?PHP echo $dbName; ?>
                                            </label>
                                        </div>                               
                                <?PHP } ?>
                                
                                
                            </div>

                            <div class="form-group">
                                <label>Food Image</label>
                                <input class="form-control" type="file" name="fileupload"/>
                            </div>

                            <button type="submit" class="btn btn-default">Create</button>

                        </form>
                    </div>
                </div>
     
<?php include '../partials/footer.php'; ?>

