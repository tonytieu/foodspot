<?php include '../partials/header.php'; ?>

<?PHP

    $conn = connectToDB();
    $result = null;

    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    
    $itemID = null;
    $dateCreated = null;
    $dateModified = null;
    $title = null;
    $price = null;
    $shortDesc = null;
    $longDesc = null;
    $imageUrl = null;
    $categoryId = null;
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $itemID = $_POST["ItemID"];
        $title = $_POST["Title"];
        $price = $_POST["Price"];
        $shortDesc = $_POST["ShortDescription"];
        $longDesc = $_POST["LongDescription"];
        $categoryId = $_POST["CategoryID"];
        $imageUrl = $_POST["ImageURL"];
        
        if(strlen($_FILES["fileupload"]["name"] > 0)) {
            $image = $_FILES["fileupload"]["tmp_name"];
            $target_dir  = '/FoodSpot/admin/upload/food';
            $target_file = $_FILES["fileupload"]["name"];

            $imagePath = $target_dir . "/" . $target_file;

            uploadImageFile($_SERVER['DOCUMENT_ROOT'] . $target_dir, $target_file);

            $imageUrl = $imagePath;
        }
        $sql = "update ITEM" .
                " set Title = '$title', Price = $price, CategoryID = $categoryId, ShortDescription = '$shortDesc', LongDescription = '$longDesc', DateModified = now(), ImageURL = '$imageUrl'" .
                " where ItemID = $itemID";

        if (mysqli_query($conn, $sql)) {
            echo "Record updated successfully";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }

    } 
    
    if (isset($_GET["ItemID"])) {
        $itemID = $_GET["ItemID"];
    }

    if ($conn && $itemID) {
        $sql = "select * from ITEM where ItemID = $itemID";

        $result = mysqli_query($conn, $sql);
    } 

    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while ($row = mysqli_fetch_assoc($result)) {
            $itemID = $row["ItemID"];
            $dateCreated = $row["DateCreated"];
            $dateModified = $row["DateModified"];
            $title = $row["Title"];
            $price = $row["Price"];
            $shortDesc = $row["ShortDescription"];
            $longDesc = $row["LongDescription"];
            $imageUrl = $row["ImageURL"];
            $categoryId = $row["CategoryID"];

        }
    }
?>

<h2>Edit <?PHP echo $title; ?></h2>

<form class="form-horizontal" action="<?PHP echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label class="control-label col-sm-2" for="ItemID">ItemID:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="ItemID" name="ItemID" value="<?PHP echo $itemID; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="DateCreated">Date Created:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" disabled id="DateCreated" name="DateCreated" value="<?PHP echo $dateCreated; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="DateModified">Date Modified:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" disabled id="DateModified" name="DateModified" value="<?PHP echo $dateModified; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="CategoryID">Category:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="CategoryID" name="CategoryID" value="<?PHP echo $categoryId; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="title">Title:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="Title" name="Title" placeholder="" value="<?PHP echo $title; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="price">Price:</label>
        <div class="col-sm-10"> 
            <input type="number" class="form-control" id="Price" name="Price" placeholder="" value="<?PHP echo $price; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="ShortDescription">Short Description:</label>
        <div class="col-sm-10"> 
            <input type="text" class="form-control" id="ShortDescription" name="ShortDescription" value="<?PHP echo $shortDesc; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="LongDescription">Long Description:</label>
        <div class="col-sm-10"> 
            <input type="text" class="form-control" id="LongDescription" name="LongDescription" value="<?PHP echo $longDesc; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="ImageURL">Main Thumbnail:</label>
        <div class="col-sm-10"> 
            <img src="<?PHP echo $imageUrl ?>" alt="<?PHP echo $imageUrl ?>" style="width: 200px; height: 200px;" />
            <input type="text" name="ImageURL" id="ImageURL" value="<?PHP echo $imageUrl ?>" />
            <input type="file" name="fileupload" id="fileupload" />
        </div>
    </div>
    <div class="form-group"> 
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </div>
</form>

<?php include '../partials/footer.php'; ?>

